⌈-⌉ lesson26: schema
    + tables
        group: table
            + columns
                [1] id: int NN auto_increment = 1
                [2] name: varchar(50)
                [3] group_row_info_id: int
            + indices
                group_row_info_id: index (group_row_info_id) type btree
            + keys
                PRIMARY: PK (id) (underlying index PRIMARY)
            + foreign-keys
                group_ibfk_1: foreign key (group_row_info_id) -> group_row_info (id)
            + triggers
                insert_group_info: trigger before row insert definer root@localhost
        group_row_info: table
            + columns
                [1] id: int NN auto_increment = 1
                [2] createDate: timestamp default CURRENT_TIMESTAMP
                [3] updateDate: timestamp default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP
                [4] status: int default 1
            + keys
                PRIMARY: PK (id) (underlying index PRIMARY)
        student: table
            + columns
                [1] id: int NN auto_increment = 1
                [2] name: varchar(50)
                [3] surname: varchar(50)
                [4] major: varchar(50)
                [5] fee: varchar(50)
                [6] startDate: date
                [7] leaveDate: date
                [8] studentRowInfoId: int
            + indices
                studentRowInfoId: index (studentRowInfoId) type btree
            + keys
                PRIMARY: PK (id) (underlying index PRIMARY)
            + foreign-keys
                student_ibfk_1: foreign key (studentRowInfoId) -> student_row_info (id)
            + triggers
                insert_student_info: trigger before row insert definer root@localhost
        student_row_info: table
            + columns
                [1] id: int NN auto_increment = 1
                [2] createDate: timestamp default CURRENT_TIMESTAMP
                [3] updateDate: timestamp default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP
                [4] status: int default 1
            + keys
                PRIMARY: PK (id) (underlying index PRIMARY)
        student_teacher_group: table
            + columns
                [1] id: int NN auto_increment = 1
                [2] group_id: int
                [3] student_id: int
                [4] teacher_id: int
            + indices
                group_id: index (group_id) type btree
                student_id: index (student_id) type btree
                teacher_id: index (teacher_id) type btree
            + keys
                PRIMARY: PK (id) (underlying index PRIMARY)
            + foreign-keys
                student_teacher_group_ibfk_1: foreign key (group_id) -> group (id)
                student_teacher_group_ibfk_2: foreign key (student_id) -> student (id)
                student_teacher_group_ibfk_3: foreign key (teacher_id) -> teacher (id)
        teacher: table
            + columns
                [1] id: int NN auto_increment = 1
                [2] name: varchar(50)
                [3] surname: varchar(50)
                [4] profession: varchar(50)
                [5] salary: double
                [6] startDate: date
                [7] leaveDate: date
                [8] teacherRowInfoId: int
            + indices
                teacherRowInfoId: index (teacherRowInfoId) type btree
            + keys
                PRIMARY: PK (id) (underlying index PRIMARY)
            + foreign-keys
                teacher_ibfk_1: foreign key (teacherRowInfoId) -> teacher_row_info (id)
            + triggers
                insert_teacher_info: trigger before row insert definer root@localhost
        teacher_row_info: table
            + columns
                [1] id: int NN auto_increment = 1
                [2] createDate: timestamp default CURRENT_TIMESTAMP
                [3] updateDate: timestamp default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP
                [4] status: int default 1
            + keys
                PRIMARY: PK (id) (underlying index PRIMARY)
